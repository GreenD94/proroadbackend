<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "email" => $this->email,
            "number" => (string)$this->number,
            "name" => $this->name,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            "address" => $this->address,
            'roles' => $this->when($this->roles->isNotEmpty(), function () {
                return RoleResource::collection($this->roles);
            }, collect([])),
            'is_verified' => !$this->sms_code,
            'avatar' => $this->when($this->avatar, function () {
                return new ImageResource($this->avatar);
            }, collect([])),
        ];
    }
}
