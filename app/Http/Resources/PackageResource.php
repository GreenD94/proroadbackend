<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "created_at" => $this->created_at,
            "chat_room_id" => (int) $this->chat_room_id,
            "description" => $this->description,
            "zip_code_pickup" => (int) $this->zip_code_pickup,
            "delivery_pickup" =>  $this->delivery_pickup,
            "driver" => $this->when($this->driver, function () {
                return new UserResource($this->driver);
            }, collect([])),
            "owner" => new UserResource($this->owner),
            "zip_code" => (int)$this->zip_code,
            "delivery_address" =>  $this->delivery_address,
            "phone" =>  (int)$this->phone,
            "note" =>  $this->note,
            "price" =>  (float)$this->price,
            'status' => $this->when($this->status, function () {
                return new StatusLogResource($this->status);
            }, collect([])),
            'images' => $this->when($this->images, function () {
                return  ImageResource::collection($this->images);
            }, collect([])),
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'longitude_pickup' => $this->longitude_pickup,
            'latitude_pickup' => $this->latitude_pickup,
        ];
    }
}
