<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatMessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "message" => $this->message,
            "is_check" => (bool) $this->is_check,
            "is_double_check" => (bool) $this->is_double_check,
            "user" => new UserResource($this->user),
            'image' => $this->when($this->image, function () {
                return new ImageResource($this->image);
            }, collect([])),
        ];
    }
}
