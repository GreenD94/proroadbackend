<?php

namespace App\Http\Controllers;

use App\Http\Requests\chat_messages\ChatMessageDestroyRequest;
use App\Http\Requests\chat_messages\ChatMessageIndexRequest;
use App\Http\Requests\chat_messages\ChatMessageStoreRequest;
use App\Http\Requests\chat_messages\ChatMessageUpdateRequest;
use App\Http\Resources\ChatMessageResource;
use App\Models\User;


use App\Http\Resources\UserResource;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Traits\Responser;

use Illuminate\Support\Facades\Hash;


class ChatMessageController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ChatMessageIndexRequest  $request)
    {
        $stocks = ChatMessage::chatRoomId($request->chat_room_id)->paginate(5);
        $data = [
            'total' => $stocks->total(),
            'per_page' => $stocks->perPage(),
            'current_page' => $stocks->currentPage(),
            'last_page' => $stocks->lastPage(),
            'next_page_url' => $stocks->nextPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'chat_messages' =>  ChatMessageResource::collection($stocks->items()),
        ];
        return $this->successResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChatMessageStoreRequest $request)
    {
        $userData = $request->only(
            'message',
            'is_check',
            'is_double_check',
            'chat_room_id',
            'user_id',
            'image_id'
        );
        $createdModel = ChatMessage::create($userData);
        $chatRoomModel = ChatRoom::find($createdModel->chat_room_id);
        $chatRoomModel->users()->syncWithoutDetaching([$createdModel->user_id]);

        if ($request->has('is_chat_users')) return $this->successResponse(UserResource::collection($chatRoomModel->users));

        return $this->successResponse(new ChatMessageResource(ChatMessage::find($createdModel->id)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ChatMessageUpdateRequest $request, User $user)
    {
        $userData = $request->only(
            'message',
            'is_check',
            'is_double_check',
            'chat_room_id',
            'user_id',
            'image_id'
        );


        ChatMessage::whereId($request->id)->update($userData);
        $model = ChatMessage::find($request->id);
        return $this->successResponse(new ChatMessageResource($model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatMessageDestroyRequest $request)
    {
        $createdModel = ChatMessage::find($request->id);
        ChatMessage::destroy($request->id);
        return $this->successResponse(new ChatMessageResource($createdModel));
    }
}
