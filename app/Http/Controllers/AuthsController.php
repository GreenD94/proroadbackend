<?php

namespace App\Http\Controllers;

use App\Http\Requests\auths\AuthIndexRequest;
use App\Http\Requests\auths\AuthStoreRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Responser;
use AppleSignIn\ASDecoder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Twilio\Rest\Client;


class AuthsController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AuthIndexRequest $request)
    {


        if ($request->has('number'))
            return $this->successResponse(['number' => true], "the number " . $request->number . " exists");;
        $authUser = User::find($request->user()->id);

        return $this->successResponse(new UserResource($authUser));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthStoreRequest $request)
    {


        if ($request->driver == "sms") {
            try {
                $result = User::StoreSMSAuth($request->token);
                if (!$result) $this->errorResponse(null, "invalid credentials", 401);
                return $this->successResponse(new UserResource($result));
            } catch (\Throwable $th) {
                return $this->errorResponse($th->getTrace(),  $th->getMessage(), 500);
            }
        }

        $result = User::StoreAuth($request->number, $request->device_key);
        if (!$result) $this->errorResponse(null, "invalid credentials", 401);

        $code =  $result["code"];
        $user = $result["user"];
        unset($result["code"]);
        $receiverNumber = '+' . ($user->number);
        $message = "Your Pro Road verification code is " . $code;
        unset($result["user"]);
        try {

            $account_sid = "AC8c6c72e9650ae76e6b1d864714e3a800";
            $auth_token = "da5a037ff0eca5a72fa0970bceae0833";
            $twilio_number = "+12193275193";
            if ($request->is_twillio) {
                $client = new Client($account_sid, $auth_token);
                $client->messages->create($receiverNumber, [
                    'from' => $twilio_number,
                    'body' => $message
                ]);
            }

            return $this->successResponse($result);
        } catch (Exception $e) {
            $this->errorResponse($e->getTrace(), "the number: " . $request->number . " is not available in twillio", 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {


        $user = User::find(Auth::id());
        $user->tokens()->delete();
        return $this->successResponse();
    }
}
