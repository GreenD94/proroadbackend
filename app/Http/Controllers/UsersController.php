<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Like;
use App\Models\Inf;
use App\Http\Controllers\InfController;
use App\Http\Requests\users\UserDestroyRequest;
use App\Http\Requests\users\UserIndexRequest;
use App\Http\Requests\users\UserStoreRequest;
use App\Http\Requests\users\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\Image;
use App\Models\InformacionUsuario;
use App\Traits\Responser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\support\Facades\Auth;
use Illuminate\support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\support\Facades\Storage;

class UsersController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserIndexRequest  $request)
    {




        $stocks = User::paginate(5);
        $data = [
            'total' => $stocks->total(),
            'per_page' => $stocks->perPage(),
            'current_page' => $stocks->currentPage(),
            'last_page' => $stocks->lastPage(),
            'next_page_url' => $stocks->nextPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'users' =>  UserResource::collection($stocks->items()),
        ];
        return $this->successResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $userData = $request->only(
            'email',
            'number',
            'name',
            'address',
            'latitude',
            'longitude'
        );

        $modelData = null;
        $createdImageModel = null;
        if ($request->has('avatar')) {
            try {
                $modelData = Image::storeImage($request->file('avatar'));
            } catch (\Throwable $th) {
                return $this->errorResponse(null, $th->getMessage());
            }
            $createdImageModel = Image::create($modelData);
            $userData['avatar_id'] = $createdImageModel->id;
        }
        $userData['sms_code'] = random_int(100000, 999999);

        $createdModel = User::create($userData);
        $createdModel->assignRole("client");
        return $this->successResponse(new UserResource(User::find($createdModel->id)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $userData = $request->only(
            'email',
            'number',
            'name',
            'address',
            'latitude',
            'longitude'
        );
        if ($request->has('avatar')) {
            try {
                $createdModel = User::find($request->id);
                Image::destroyImage($createdModel->avatar->id);
                $modelImageData = Image::storeImage($request->file('avatar'));
                Image::where("id", $createdModel->avatar_id)->update($modelImageData);
                $userData = array_merge($userData, ["avatar_id" => $createdModel->avatar_id]);
            } catch (\Throwable $th) {
                return $this->errorResponse($th->getTrace(), $th->getMessage());
            }
        }

        User::whereId($request->id)->update($userData);
        $model = User::find($request->id);
        return $this->successResponse(new UserResource($model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDestroyRequest $request)
    {
        $createdModel = User::find($request->id);
        User::destroy($request->id);
        return $this->successResponse(new UserResource($createdModel));
    }
}
