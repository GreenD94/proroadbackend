<?php

namespace App\Http\Controllers;

use App\Http\Requests\pushnotification\PushNotificationStoreRequest;
use App\Models\User;
use App\Traits\Responser;
use Illuminate\Http\Request;

class PushNotificationController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PushNotificationStoreRequest $request)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $FcmToken = null;
        if ($request->to == "*") {
            $FcmToken = User::whereNotNull('device_key')->pluck('device_key')->all();
        } else {
            $FcmToken = User::whereIn('id', $request->to)->whereNotNull('device_key')->pluck('device_key')->all();
        }

        if (empty($FcmToken)) return $this->errorResponse($request->to,  "validation error: the users with these ids do not have a device key or they do not exits in database", 422);;


        $serverKey = 'AAAAkwgF9wM:APA91bEmeFKliClnr3UqMN92hwtmAzRRqqVfcmboKOukvu7WS596Ti4n75ZGhbVd9z4nS__b-Mk-x8_6okOC_rHruSGdhus3oXZO3_Qt1nkmxreDgPV3uekJk9Qz1nr2IYf7-D8Q9W28';

        $data = [
            "registration_ids" => $FcmToken,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,
            ],
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            return $this->errorResponse($ch,  "error al conectar con https://fcm.googleapis.com/fcm/send", 500);
        }
        // Close connection
        curl_close($ch);
        return $this->successResponse($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
