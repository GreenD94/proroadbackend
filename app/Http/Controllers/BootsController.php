<?php

namespace App\Http\Controllers;

use App\Events\WebSocketDemoEvent;
use App\Http\Requests\boots\BootIndexRequest;
use App\Http\Requests\categories\CategoryDeleteRequest;
use App\Http\Requests\categories\CategoryIndexRequest;
use App\Http\Requests\categories\CategoryStoreRequest;
use App\Http\Requests\categories\CategoryUpdateRequest;
use App\Http\Resources\BannerResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\StockResource;
use App\Http\Resources\UserResource;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Stock;
use App\Models\User;
use App\Traits\Responser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class BootsController extends Controller
{
    use Responser;


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
    }


    public function refresh()
    {
        try {
            Artisan::call('migrate:fresh');
            Artisan::call('db:seed');
        } catch (\Throwable $th) {
            return $this->errorResponse($th->getTrace()[0], $th->getMessage(), 500);
        }


        return $this->successResponse([], "success");
    }
}
