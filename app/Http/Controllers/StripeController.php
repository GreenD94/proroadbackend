<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe;
use Illuminate\Support\Facades\Session;


class StripeController extends Controller
{
    /**
     * payment view
     */
    public function handleGet()
    {
        return view('checkoutstripe');
    }

    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {



        Stripe\Stripe::setApiKey("sk_live_QbJQaDd8FkcRdPb8FSNm777m");
        $result =  Stripe\Charge::create([
            "amount" => 100 * 10,
            "currency" => "USD",
            "source" => $request->stripeToken,
            "description" => "Making test payment."
        ]);

        Session::flash('success', 'Payment has been successfully processed.');

        return back();
    }
}
