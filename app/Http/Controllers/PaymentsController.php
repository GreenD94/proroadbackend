<?php

namespace App\Http\Controllers;

use App\Traits\Responser;
use Illuminate\Http\Request;
use Nikolag\Square\Facades\Square;
use Stripe;
use Illuminate\Support\Facades\Session;

class PaymentsController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->driver == "stripe") {
            try {
                Stripe\Stripe::setApiKey("pk_live_QLF5AxnPyxg5ld5XNqUvpHri");
                $result =  Stripe\Charge::create([
                    "amount" => 100 * 150,
                    "currency" => "EUR",
                    "source" => $request->token,
                    "description" => "Making test payment."
                ]);
                return $this->successResponse();
            } catch (\Throwable $th) {
                return $this->errorResponse($th->getTrace(),  $th->getMessage(), 500);
            }
        }
        if ($request->driver == "square") {
            try {
                $transaction = Square::charge([
                    "amount" => 5000,
                    "source_id" => $request->token,
                    "location_id" => "LYBBFDPS78499",
                    "currency" => "USD"

                ]);
                return  $this->successResponse();
            } catch (\Throwable $th) {
                return $this->errorResponse($th->getTrace(),  $th->getMessage(), 500);
            }
        }
        $this->errorResponse([], "driver debe contener el string square o stripe", 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
