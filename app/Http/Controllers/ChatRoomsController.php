<?php

namespace App\Http\Controllers;

use App\Http\Requests\chat_rooms\ChatRoomDestroyRequest;
use App\Http\Requests\chat_rooms\ChatRoomIndexRequest;
use App\Http\Requests\chat_rooms\ChatRoomStoreRequest;
use App\Http\Requests\chat_rooms\ChatRoomUpdateRequest;
use App\Models\User;



use App\Http\Resources\ChatRoomResource;

use App\Models\ChatRoom;

use App\Traits\Responser;

use Illuminate\Support\Facades\Hash;


class ChatRoomsController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ChatRoomIndexRequest  $request)
    {
        $stocks = ChatRoom::userId($request->user_id)->paginate(5);
        $data = [
            'total' => $stocks->total(),
            'per_page' => $stocks->perPage(),
            'current_page' => $stocks->currentPage(),
            'last_page' => $stocks->lastPage(),
            'next_page_url' => $stocks->nextPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'chat_rooms' =>  ChatRoomResource::collection($stocks->items()),
        ];
        return $this->successResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChatRoomStoreRequest $request)
    {
        $userData = $request->only(
            'is_active',
        );



        $createdModel = ChatRoom::create($userData);


        return $this->successResponse(new ChatRoomResource(ChatRoom::find($createdModel->id)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ChatRoomUpdateRequest $request, User $user)
    {
        $userData = $request->only(
            'is_active',
        );


        ChatRoom::whereId($request->id)->update($userData);
        $model = ChatRoom::find($request->id);
        return $this->successResponse(new ChatRoomResource($model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatRoomDestroyRequest $request)
    {
        $createdModel = ChatRoom::find($request->id);
        ChatRoom::destroy($request->id);
        return $this->successResponse(new ChatRoomResource($createdModel));
    }
}
