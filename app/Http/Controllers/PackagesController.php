<?php

namespace App\Http\Controllers;

use App\Http\Requests\packages\PackageDestroyRequest;
use App\Http\Requests\packages\PackageIndexRequest;
use App\Http\Requests\packages\PackageStoreRequest;
use App\Http\Requests\packages\PackageUpdateRequest;
use App\Http\Resources\PackageResource;
use App\Models\User;
use App\Models\Package;
use App\Models\Statu;
use App\Models\StatusLog;
use App\Traits\Responser;

use Illuminate\Support\Facades\Hash;


class PackagesController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PackageIndexRequest  $request)
    {
        $stocks = Package::whenOwnerId($request->owner_id)->whenStatusId($request->status_id)
            ->orderBy('id', 'desc')->paginate($request->limit ?? 5);
        $data = [
            'total' => (int) $stocks->total(),
            'per_page' => (int) $stocks->perPage(),
            'current_page' => (int)  $stocks->currentPage(),
            'last_page' => (int)  $stocks->lastPage(),
            'next_page_url' => $stocks->nextPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'packages' =>  PackageResource::collection($stocks->items()),
        ];
        return $this->successResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageStoreRequest $request)
    {
        $userData = $request->only(
            'chat_room_id',
            'driver_id',
            'owner_id',
            'zip_code',
            'longitude',
            'latitude',
            'delivery_address',
            'phone',
            'note',
            'price',
            'description',
            'zip_code_pickup',
            'delivery_pickup',
            'longitude_pickup',
            'latitude_pickup',
        );


        $createdModel = Package::create($userData);
        $createdModel->status_logs()->create(["statu_id" => Statu::find(1)->id]);

        return $this->successResponse(new PackageResource(Package::find($createdModel->id)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageUpdateRequest $request)
    {

        $latest_log = StatusLog::where('package_id', $request->id)->latest()->first()?->statu_id;
        if ($latest_log != 1) return $this->errorResponse(null, "Unauthorized: you do not have the permissions to edit an Package with status of " . ($latest_log?->status?->name ?? "no status"), 423);

        $userData = $request->only(
            'chat_room_id',
            'driver_id',
            'owner_id',
            'zip_code',
            'delivery_address',
            'phone',
            'longitude',
            'latitude',
            'note',
            'price',
            'description',
            'zip_code_pickup',
            'delivery_pickup',
            'longitude_pickup',
            'latitude_pickup',
        );
        if ($request->has('driver_id')) {
            $currentPackage = Package::find($request->id);
            if (!$currentPackage->owner_id) $currentPackage->status_logs()->create(["statu_id" => Statu::find(2)->id]);;
        }

        Package::whereId($request->id)->update($userData);
        $model = Package::find($request->id);
        return $this->successResponse(new PackageResource($model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageDestroyRequest $request)
    {
        $createdModel = Package::find($request->id);
        Package::destroy($request->id);
        return $this->successResponse(new PackageResource($createdModel));
    }
}
