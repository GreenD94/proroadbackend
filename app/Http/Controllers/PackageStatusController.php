<?php

namespace App\Http\Controllers;

use App\Http\Requests\status\StatuDestroyRequest;
use App\Http\Requests\status\StatuIndexRequest;
use App\Http\Requests\status\StatuStoreRequest;
use App\Http\Requests\status\StatuUpdateRequest;
use App\Http\Resources\StatuResource;

use App\Models\Statu;
use App\Traits\Responser;




class PackageStatusController extends Controller
{
    use Responser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StatuIndexRequest  $request)
    {
        $stocks = Statu::paginate(5);
        $data = [
            'total' => $stocks->total(),
            'per_page' => $stocks->perPage(),
            'current_page' => $stocks->currentPage(),
            'last_page' => $stocks->lastPage(),
            'next_page_url' => $stocks->nextPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'prev_page_url' => $stocks->previousPageUrl(),
            'package_status' =>  StatuResource::collection($stocks->items()),
        ];
        return $this->successResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StatuStoreRequest $request)
    {
        $userData = $request->only(
            'name',
        );


        $createdModel = Statu::create($userData);

        return $this->successResponse(new StatuResource(Statu::find($createdModel->id)));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StatuUpdateRequest $request)
    {
        $userData = $request->only(
            'name',
        );


        Statu::whereId($request->id)->update($userData);
        $model = Statu::find($request->id);
        return $this->successResponse(new StatuResource($model));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatuDestroyRequest $request)
    {
        $createdModel = Statu::find($request->id);
        Statu::destroy($request->id);
        return $this->successResponse(new StatuResource($createdModel));
    }
}
