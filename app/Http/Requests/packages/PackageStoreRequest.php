<?php

namespace App\Http\Requests\packages;

use App\Traits\Responser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;


class  PackageStoreRequest extends FormRequest
{
    use Responser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => ['required'],
            'price' =>  ['required', 'numeric'],
            'phone' =>  ['required'],
            'delivery_address' =>  ['required'],
            'zip_code' =>  ['numeric', 'gte:1'],
            'chat_room_id' =>  ['exists:chat_rooms,id', 'numeric', 'gte:1'],
            'owner_id' =>  ['required', 'exists:users,id', 'numeric', 'gte:1'],
            'driver_id' =>  ['exists:users,id', 'numeric', 'gte:1'],
            'delivery_pickup' =>  ['required'],
            'zip_code_pickup' =>  ['numeric', 'gte:1'],


        ];
    }
    protected  function failedValidation(Validator $validator)
    {
        $this->errorResponse($validator->errors(), "Validation Error", 422);
    }
}
