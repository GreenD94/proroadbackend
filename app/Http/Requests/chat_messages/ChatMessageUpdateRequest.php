<?php

namespace App\Http\Requests\chat_messages;

use App\Traits\Responser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class ChatMessageUpdateRequest extends FormRequest
{
    use Responser;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id' =>  ['required', 'exists:chat_messages,id', 'numeric', 'gte:1'],
            //'message' =>  ['required'],
            'is_check' =>  ['boolean'],
            'is_double_check' =>  ['boolean'],
            'chat_room_id' =>  ['exists:chat_rooms,id', 'numeric', 'gte:1'],
            'user_id' =>  ['exists:users,id', 'numeric', 'gte:1'],
            'image_id' =>  ['exists:images,id', 'numeric', 'gte:1'],
        ];
    }

    protected  function failedValidation(Validator $validator)
    {
        $this->errorResponse($validator->errors(), "Validation Error", 422);
    }
}
