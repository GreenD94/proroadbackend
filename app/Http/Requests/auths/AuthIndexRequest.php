<?php

namespace App\Http\Requests\auths;

use App\Traits\Responser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;


class AuthIndexRequest extends FormRequest
{
    use Responser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        return [
            'number' =>  ["exists:users,number", 'numeric', 'gte:1'],
        ];
    }
    protected  function failedValidation(Validator $validator)
    {
        // $alphabet = range('a', 'z');
        // // $errors = $validator->errors()->getMessages();
        // foreach ($errors as $error => $messages) {
        //     $newMessages = [];
        //     foreach ($messages as $key => $value) {


        //         $newMessages[Hash::make($value)] = $value;
        //     }
        //     $errors[$error] = $newMessages;
        // }
        $this->errorResponse($validator->errors(), "Validation Error", 422);
    }
}
