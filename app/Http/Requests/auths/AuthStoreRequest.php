<?php

namespace App\Http\Requests\auths;

use App\Traits\Responser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;


class AuthStoreRequest extends FormRequest
{
    use Responser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->driver == "sms" && (!$this->bearerToken())) $this->errorResponse(null, 'Unauthenticated', 401);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->driver == "sms") return [
            'token' =>  ['required'],
            'driver' =>  [Rule::in(['sms'])],
        ];


        return [
            'number' =>  ['required', "exists:users,number", 'numeric', 'gte:1', 'integer'],
            'device_key' =>  ['required'],
            'driver' =>  [Rule::in(['sms'])],
        ];
    }
    protected  function failedValidation(Validator $validator)
    {
        $this->errorResponse($validator->errors(), "Validation Error", 422);
    }
}
