<?php

namespace App\Http\Requests\pushnotification;

use App\Traits\Responser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class PushNotificationStoreRequest extends FormRequest
{
    use Responser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $result =   [
            'to' =>  ['required'],
            'title' =>  ['required'],
            'body' =>  ['required'],
        ];
        if ($this->to == "*") {
            $result["to"] = ["required",  Rule::in(['*']),];
        } else {
            $result["to.*"] = ['exists:users,id', 'numeric', 'gte:1'];
            $result["to"] = ["required",  "array"];
        }

        return $result;
    }

    protected  function failedValidation(Validator $validator)
    {
        $this->errorResponse($validator->errors(), "Validation Error", 422);
    }
}
