<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusLog extends Model
{
    use HasFactory;
    protected $with = ['status'];
    protected $fillable = [
        'package_id',
        'statu_id',
    ];
    public function status()
    {
        return $this->belongsTo(Statu::class, 'statu_id', 'id');
    }
}
