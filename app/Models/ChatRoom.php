<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ChatRoom extends Model
{
    use HasFactory;
    protected $fillable = [
        'is_active',
    ];
    public function message()
    {
        return $this->hasMany(ChatMessage::class, 'chat_room_id', 'id');
    }
    public function packages()
    {
        return $this->hasMany(Package::class, 'chat_room_id', 'id');
    }

    public function scopeUserId($query, $user_id)
    {
        if ($user_id) {
            $query->whereHas('message', function (Builder $query1) use ($user_id) {
                $query1->where("user_id", $user_id);
            });
            $query->orWhereHas('packages', function (Builder $query1) use ($user_id) {
                $query1->where("owner_id", $user_id);
                $query1->orWhere("driver_id", $user_id);
            });
        };
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'chat_users', 'Chat_room_id', 'user_id')->orderBy('id');
    }
}
