<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    use HasFactory;
    protected $with = ['user', 'image'];
    protected $fillable = [
        'message',
        'is_check',
        'is_double_check',
        'chat_room_id',
        'user_id',
        'image_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id', 'id');
    }

    public function chat_room()
    {
        return $this->belongsTo(ChatRoom::class, 'chat_room_id', 'id');
    }

    public function scopeChatRoomId($query, $chat_room_id)
    {
        if ($chat_room_id) $query->where('chat_room_id', $chat_room_id);
    }
}
