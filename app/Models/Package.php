<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Package extends Model
{
    use HasFactory;
    protected $with = ['status_logs.status', 'images'];
    protected $appends = ['status'];
    protected $fillable = [
        'chat_room_id',
        'driver_id',
        'owner_id',
        'zip_code',
        'delivery_address',
        'phone',
        'note',
        'price',
        'description',
        'zip_code_pickup',
        'delivery_pickup',
        'longitude',
        'latitude',
        'longitude_pickup',
        'latitude_pickup',
    ];

    public function chat_room()
    {
        return $this->belongsTo(ChatRoom::class, 'chat_room_id', 'id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id', 'id');
    }
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'package_images', 'package_id', 'image_id');
    }

    public function status_logs()
    {
        return $this->hasMany(StatusLog::class, 'package_id', 'id');
    }
    public function getStatusAttribute()
    {
        return $this->status_logs->last();
    }
    public function scopeWhenOwnerId($query, $owner_id)
    {
        if ($owner_id) $query->where('owner_id', $owner_id);
    }

    public function scopeWhenStatusId($query, $status_id)
    {
        if ($status_id) $query->where(function ($query) {
            $query->select('statu_id')
                ->from('status_logs')
                ->whereColumn('status_logs.package_id', 'packages.id')
                ->orderBy('status_logs.id', 'desc')
                ->limit(1);
        }, $status_id);
        // if ($status_id) $query->whereHas('status_logs', function (Builder $query) use ($status_id) {
        //     $query->whereColumn('status_logs.id', $status_id)->limit(1);
        // });
    }
}
