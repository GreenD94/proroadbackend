<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Nikolag\Square\Traits\HasCustomers;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasCustomers;
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;

    protected $with = ['avatar', 'roles'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'address',
        'device_key',
        'number',
        'sms_code',
        'avatar_id',
        'latitude',
        'longitude'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function avatar()
    {
        return $this->belongsTo(Image::class, 'avatar_id', 'id');
    }


    static public function StoreAuth($number, $device_key)
    {
        $user = User::where('number', $number)->first();

        if ($device_key) $user->device_key = $device_key;
        if ($device_key) $user->save();
        if (!$user) {
            return false;
        }
        $user->sms_code = random_int(100000, 999999);
        $user->save();
        return ['token' => $user->createToken($user->id . $user->name . uniqid())->plainTextToken, "code" => $user->sms_code, 'user' => $user];
    }

    static public function StoreSMSAuth($token)
    {

        $user = User::find(Auth()->id());
        if ($user->sms_code != $token) return false;
        $user->sms_code = null;
        $user->save();

        return   $user;
    }
}
