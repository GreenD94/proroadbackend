<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string("delivery_pickup");
            $table->string("zip_code_pickup")->nullable();;
            $table->string("zip_code")->nullable();;
            $table->string("delivery_address");
            $table->string("phone");
            $table->string("description");
            $table->string("note")->default("");
            $table->decimal("price");

            $table->string("longitude")->nullable();
            $table->string("latitude")->nullable();
            $table->string("longitude_pickup")->nullable();
            $table->string("latitude_pickup")->nullable();


            $table->unsignedBigInteger('chat_room_id')->nullable();
            $table->foreign('chat_room_id')->references('id')->on('chat_rooms');

            $table->unsignedBigInteger('driver_id')->nullable();
            $table->foreign('driver_id')->references('id')->on('users');

            $table->unsignedBigInteger('owner_id');
            $table->foreign('owner_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
