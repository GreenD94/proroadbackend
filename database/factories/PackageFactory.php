<?php

namespace Database\Factories;

use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PackageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            "chat_room_id" => ChatRoom::factory(),
            "driver_id" => User::factory(),
            "owner_id" => User::factory(),
            "zip_code" => $this->faker->randomDigit(),
            "delivery_address" => $this->faker->address(),
            "phone" => $this->faker->randomNumber(),
            "note" => $this->faker->sentence(),
            "price" => $this->faker->randomDigit(),
            "description" => $this->faker->sentence(),
            'zip_code_pickup' => $this->faker->randomDigit(),
            'delivery_pickup' => $this->faker->address(),
            'longitude' => $this->faker->longitude(),
            'latitude' => $this->faker->latitude(),
            'longitude_pickup' => $this->faker->longitude(),
            'latitude_pickup' => $this->faker->latitude(),
        ];
    }
}
