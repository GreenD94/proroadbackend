<?php

namespace Database\Factories;

use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChatMessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "message" => $this->faker->sentence(),
            "is_check" => false,
            "is_double_check" => false,
            "chat_room_id" => ChatRoom::factory(),
            "user_id" => User::factory(),
            'image_id' => null
        ];
    }
}
