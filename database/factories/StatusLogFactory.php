<?php

namespace Database\Factories;

use App\Models\Package;
use App\Models\Statu;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusLogFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "package_id" => Package::factory(),
            "status_id" => Statu::factory(),
        ];
    }
}
