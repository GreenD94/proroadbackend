<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'number' => random_int(100000, 999999),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'sms_code' => null,
            'address' =>     $this->faker->unique()->address(),
            'avatar_id' => Image::factory(),
            'remember_token' => Str::random(10),
            'device_key' => null,
            'latitude' => $this->faker->longitude(),
            'longitude' => $this->faker->latitude(),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
