<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Color;
use App\Models\Image;
use App\Models\Package;
use App\Models\Size;
use App\Models\Statu;
use App\Models\Stock;
use App\Models\User;
use App\Models\Video;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class MasterUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $body = [
            'number' => 123456,
        ];
        $user = User::factory()->create($body);
        Statu::factory()->create(['name' => 'pending']);
        Statu::factory()->create(['name' => 'processing']);
        Statu::factory()->create(['name' => 'finished']);

        $client = Role::create(["name" => "client"]);
        Role::create(["name" => "driver"]);
        Role::create(["name" => "admin"]);
        Role::create(["name" => "external"]);
        $user->assignRole("client");

        $driver = User::factory()->create([
            'number' => 2222,
        ]);
        $driver->assignRole("driver");

        $user2 = User::factory()->create([
            'number' => 44444,
        ]);
        $user2->assignRole("client");

        $package1 = Package::factory()->create([
            "driver_id" => null,
            "owner_id" =>  $user->id
        ]);
        $package1->status_logs()->create(["statu_id" => Statu::find(1)->id]);

        $package2 = Package::factory()->create([
            "driver_id" => $driver->id,
            "owner_id" =>  $user2->id
        ]);
        $package2->status_logs()->create(["statu_id" => Statu::find(1)->id]);
        $package2->status_logs()->create(["statu_id" => Statu::find(2)->id]);

        $package2->images()->create([
            "name" => 'aaa',
            "url" => 'https://st2.depositphotos.com/1037987/10994/i/450/depositphotos_109941700-stock-photo-man-collecting-parcel-delivery.jpg'
        ]);
        $package2->images()->create([
            "name" => 'bbb',
            "url" => 'https://static9.depositphotos.com/1669785/1150/i/950/depositphotos_11506024-stock-photo-package.jpg'
        ]);
        $package2->images()->create([
            "name" => 'ccc',
            "url" => 'https://st.depositphotos.com/1006017/1257/i/950/depositphotos_12570617-stock-photo-woman-receiving-parcel.jpg'
        ]);


        $package1->images()->create([
            "name" => 'aaa1',
            "url" => 'https://st2.depositphotos.com/1037987/10994/i/450/depositphotos_109941700-stock-photo-man-collecting-parcel-delivery.jpg'
        ]);
        $package1->images()->create([
            "name" => 'bbb1',
            "url" => 'https://static9.depositphotos.com/1669785/1150/i/950/depositphotos_11506024-stock-photo-package.jpg'
        ]);
        $package1->images()->create([
            "name" => 'ccc1',
            "url" => 'https://st.depositphotos.com/1006017/1257/i/950/depositphotos_12570617-stock-photo-woman-receiving-parcel.jpg'
        ]);
    }
}
