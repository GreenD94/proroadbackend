<?php

use App\Events\WebSocketDemoEvent;
use App\Http\Controllers\AuthsController;
use App\Http\Controllers\BootsController;
use App\Http\Controllers\ChatMessageController;
use App\Http\Controllers\ChatRoomsController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\PackagesController;
use App\Http\Controllers\PackageStatusController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\PushNotificationController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'auth:sanctum'], function () {


    Route::get('mobile/users', [UsersController::class, 'index'])->name('api.mobile.users.index');
    Route::put('mobile/users', [UsersController::class, 'update'])->name('api.mobile.users.update');
    Route::delete('mobile/users', [UsersController::class, 'destroy'])->name('api.mobile.users.destroy');

    Route::post('mobile/chat-rooms', [ChatRoomsController::class, 'store'])->name('api.mobile.chat_rooms.store');
    Route::get('mobile/chat-rooms', [ChatRoomsController::class, 'index'])->name('api.mobile.chat_rooms.index');
    Route::put('mobile/chat-rooms', [ChatRoomsController::class, 'update'])->name('api.mobile.chat_rooms.update');
    Route::delete('mobile/chat-rooms', [ChatRoomsController::class, 'destroy'])->name('api.mobile.chat_rooms.destroy');

    Route::post('mobile/chat-messages', [ChatMessageController::class, 'store'])->name('api.mobile.chat_messages.store');
    Route::get('mobile/chat-messages', [ChatMessageController::class, 'index'])->name('api.mobile.chat_messages.index');
    Route::put('mobile/chat-messages', [ChatMessageController::class, 'update'])->name('api.mobile.chat_messages.update');
    Route::delete('mobile/chat-messages', [ChatMessageController::class, 'destroy'])->name('api.mobile.chat_messages.destroy');

    Route::post('mobile/packages', [PackagesController::class, 'store'])->name('api.mobile.packages.store');
    Route::get('mobile/packages', [PackagesController::class, 'index'])->name('api.mobile.packages.index');
    Route::put('mobile/packages', [PackagesController::class, 'update'])->name('api.mobile.packages.update');
    Route::delete('mobile/packages', [PackagesController::class, 'destroy'])->name('api.mobile.packages.destroy');

    Route::post('mobile/package-status', [PackageStatusController::class, 'store'])->name('api.mobile.package_status.store');
    Route::get('mobile/package-status', [PackageStatusController::class, 'index'])->name('api.mobile.package_status.index');
    Route::put('mobile/package-status', [PackageStatusController::class, 'update'])->name('api.mobile.package_status.update');
    Route::delete('mobile/package-status', [PackageStatusController::class, 'destroy'])->name('api.mobile.package_status.destroy');

    Route::get('mobile/images', [ImagesController::class, 'index'])->name('api.mobile.images.index');
    Route::delete('mobile/images', [ImagesController::class, 'destroy'])->name('api.mobile.images.destroy');
    Route::post('mobile/images', [ImagesController::class, 'store'])->name('api.mobile.images.store');
    Route::put('mobile/images', [ImagesController::class, 'update'])->name('api.mobile.images.Update');


    Route::post('payments', [PaymentsController::class, 'store'])->name('api.payments.store');


    Route::post('mobile/push-notification', [PushNotificationController::class, 'store'])->name('api.mobile.push-notification.store');


    Route::delete('mobile/auth', [AuthsController::class, 'destroy'])->name('api.mobile.auth.destroy');
});



//-------------------------------
Route::post('mobile/users', [UsersController::class, 'store'])->name('api.mobile.users.store');
Route::get('/send', [BootsController::class, 'websocketOn']);
Route::get('mobile/refresh', [BootsController::class, 'refresh'])->name('api.mobile.boots.refresh');

Route::group(['middleware' => 'EnsureTokenIsValid'], function () {
    Route::get('mobile/auth', [AuthsController::class, 'index'])->name('api.mobile.auth.index');
    Route::post('mobile/auth', [AuthsController::class, 'store'])->name('api.mobile.auth.store');
});
