<?php

namespace Tests\Feature\auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthDeleteTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkDestroy();
    }
    public function checkData()
    {
        $body = [
            'number' => 11111111,
            "device_key" => "ssss"
        ];
        $model = User::factory()->create($body);
        $this->assertDatabaseCount('users', 1);
        $response = $this->deleteJson(route('api.mobile.auth.destroy'));
        $response->assertStatus(401);

        $response = $this->post(route("api.mobile.auth.store"), $body);
        $response->assertStatus(200);
        $this->rawToken = $response->original["data"]["token"];
    }
    public function checkDestroy()
    {

        $response = $this->deleteJson('/api/mobile/auth', [], ['Authorization' => 'Bearer ' . $this->rawToken]);
        $response->assertStatus(200);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has('data')
            );
        $this->app->get('auth')->forgetGuards();

        $response = $this->deleteJson('/api/mobile/auth', [], ['Authorization' => 'Bearer ' . $this->rawToken]);
        $response->assertStatus(401);
    }
}
