<?php

namespace Tests\Feature\auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthIndexTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkIndex();
        $this->checkNumber();
        $this->checkNumberExists();
    }

    public function checkData()
    {
        $this->data = ["number" => 33333];
        $this->authdUser = User::factory()->create($this->data);
        Sanctum::actingAs(
            $this->authdUser,
            ['*']
        );
    }

    public function checkNumberExists()
    {
        $params = array('number' => 222222522);
        $response = $this->getJson(route("api.mobile.auth.index", $params));

        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }


    public function checkIndex()
    {

        $response = $this->getJson(route("api.mobile.auth.index"));
        $response->assertStatus(200);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('email')
                            ->has('name')
                            ->has('address')
                            ->has('avatar')
                            ->has('number')
                            ->has('is_verified')
                            ->has("roles")
                            ->has('latitude')
                            ->has('longitude')
                    )
            );
    }

    public function checkNumber()
    {
        $response = $this->getJson(route("api.mobile.auth.index", $this->data));

        $response->assertStatus(200);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->where('number', true)
                    )
            );
    }
}
