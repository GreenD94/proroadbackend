<?php

namespace Tests\Feature\auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class AuthStoreValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkNumberExists();
        $this->checkNumberIsNumeric();
        $this->checkNumberIsIntenger();
        $this->checkNumberIsNatural();
        $this->checkNumberisRequiredIfDriverIsNull();

        //        $this->checkTokenRequiredIfDriverIsSMS();
        $this->checkDriverIsValid();
        $this->checkDeviceKeyRequired();
    }
    public function checkData()
    {
        $this->data = ["number" => 1111111];
        $this->model = User::factory()->create($this->data);
        $response = $this->getJson(route('api.mobile.packages.index'));
        $response->assertStatus(401);
    }
    public function checkNumberExists()
    {
        $params = array('number' => 22222222, "device_key" => "sss");
        $response = $this->post(route("api.mobile.auth.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }

    public function checkNumberIsNumeric()
    {
        $params = array('number' => 'aaaaa', "device_key" => "sss");
        $response = $this->post(route("api.mobile.auth.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }

    public function checkNumberIsIntenger()
    {
        $params = array('number' => 1.1, "device_key" => "sss");
        $response = $this->post(route("api.mobile.auth.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }

    public function checkNumberIsNatural()
    {
        $params = array('number' => -1, "device_key" => "sss");
        $response = $this->post(route("api.mobile.auth.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }
    public function checkNumberisRequiredIfDriverIsNull()
    {
        $params = array("device_key" => "sss");
        $response = $this->post(route("api.mobile.auth.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }



    public function checkTokenRequiredIfDriverIsSMS()
    {
        $params = array("driver" => "sms", "device_key" => "sss");
        $response = $this->post(route("api.mobile.auth.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('token')
                    )
            );
    }
    public function checkDriverIsValid()
    {
        $params = array("token" => "ssssssssssss", "driver" => "wwwwww", "device_key" => "sss", "number" => 1111111);
        $response = $this->post(route("api.mobile.auth.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver')
                    )
            );
    }


    public function checkDeviceKeyRequired()
    {
        $params = array("number" => $this->data['number']);
        $response = $this->post(route("api.mobile.auth.store"), $params);

        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('device_key')
                    )
            );
    }
}
