<?php

namespace Tests\Feature\auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class AuthStoreTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $this->checkData();
        $this->checkStore();
        //$this->checkSMSStore();
    }

    public function checkData()
    {
        $this->smsBody = [
            'token' => '',
            'driver' => 'sms'
        ];
        $this->body = [
            'number' => random_int(100000, 999999),
            "device_key" => "ssss"
        ];
        $this->model = User::factory()->create($this->body);

        $this->assertDatabaseCount('users', 1);
        $this->assertDatabaseHas('users',   $this->body);

        $response = $this->getJson(route('api.mobile.packages.index', ['page' => 1]));
        $response->assertStatus(401);
    }
    public function checkStore()
    {

        $response = $this->post(route("api.mobile.auth.store"),  $this->body);
        $response->assertStatus(200);
        $this->assertNotNull(User::first()->sms_code);
        $rawToken = $response->original["data"]["token"];
        $this->model->refresh();
        $this->assertDatabaseCount('personal_access_tokens', 1);
        $query = DB::table('personal_access_tokens')->where('id',  $this->model->tokens[0]->id);
        $personal_access_token =  $query->first();
        $this->assertDatabaseHas('personal_access_tokens', (array) $personal_access_token);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json->has('token')
                    )
            );

        $response = $this->getJson(route('api.mobile.packages.index', ["page" => 1]), ['Authorization' => 'Bearer ' . $rawToken]);
        $response->assertStatus(200);
    }

    public function checkSMSStore()
    {
        $this->smsBody['token'] = User::first()->sms_code;
        $response = $this->post(route("api.mobile.auth.store"), $this->smsBody);
        $response->assertStatus(200);
        $this->assertNull(User::first()->sms_code);

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json
                            ->has('id')
                            ->has('email')
                            ->has('name')
                            ->has('avatar')
                            ->has('address')
                            ->where('number',  $this->body['number'])
                            ->where('is_verified', true)
                            ->has("roles")
                    )
            );
    }
}
