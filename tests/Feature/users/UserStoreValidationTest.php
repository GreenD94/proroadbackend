<?php

namespace Tests\Feature\users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class UserStoreValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();


        $this->checkEmailUnique();
        $this->checkEmailValid();
        $this->checkAvatarisImage();


        $this->checkNumberRequired();
        $this->checkNumberUnique();
        $this->checkNumberIsNumeric();

        $this->checkNumberIsNatural();
    }
    public function checkData()
    {

        $this->data = ['number' => 222222, "email" => "test2@test.com"];
        $usersUser = User::factory()->create($this->data);
        Sanctum::actingAs(
            $usersUser,
            ['*']
        );
    }


    public function checkEmailUnique()
    {
        $params = array("email" => $this->data['email'], 'number' => 111111);
        $response = $this->postJson(route('api.mobile.users.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('email')
                    )
            );
    }
    public function checkAvatarisImage()
    {
        $params = array('number' => 111111, 'avatar' => "sssss");
        $response = $this->postJson(route('api.mobile.users.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('avatar')
                    )
            );
    }
    public function checkEmailValid()
    {
        $params = array("email" => "sssssssss", 'number' => 111111);
        $response = $this->postJson(route('api.mobile.users.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('email')
                    )
            );
    }



    public function checknumberRequired()
    {
        $params = array("email" => "test2@test2.com");
        $response = $this->postJson(route('api.mobile.users.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }


    public function checkNumberUnique()
    {
        $params = array('number' => $this->data['number']);
        $response = $this->post(route("api.mobile.users.store"), $params);
        $response->assertUnprocessable();

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }

    public function checkNumberIsNumeric()
    {
        $params = array('number' => 'aaaaa');
        $response = $this->post(route("api.mobile.users.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }


    public function checkNumberIsNatural()
    {
        $params = array('number' => -1);
        $response = $this->post(route("api.mobile.users.store"), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('number')
                    )
            );
    }
}
