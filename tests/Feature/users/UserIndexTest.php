<?php

namespace Tests\Feature\users;

use App\Models\InformacionUsuario;
use App\Models\User;
use App\Models\Userinformation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use SebastianBergmann\Environment\Console;
use Tests\TestCase;

class UserIndexTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_example()
    {
        $this->checkData();
        $this->checkPagination();
    }
    public function checkData()
    {

        $userData =
            [
                "email" => $this->faker()->email(),
                "name" => $this->faker()->name(),
                "number" => random_int(100000, 999999),
                "address" => $this->faker()->address(),
            ];
        $this->data = $userData;
        $userModel = User::factory()->create($userData);
        $users = User::factory()->count(19)->create();
        Sanctum::actingAs(
            $userModel,
            ['*']
        );

        $this->assertDatabaseHas('users', $userData);
        $this->assertDatabaseCount('users', 20);
    }

    public function checkPagination(string $page = "1", array $params = [], $expected_size = 5, $total = 20)
    {
        $params["page"] = $page;
        $response = $this->getJson(route('api.mobile.users.index', $params));

        $response->assertStatus(200);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json->has('total')
                            ->where('total', $total)
                            ->has('per_page')
                            ->has('current_page')
                            ->has('last_page')
                            ->has('next_page_url')
                            ->has('prev_page_url')
                            ->has(
                                'users',
                                $expected_size,
                                fn ($json) =>
                                $json
                                    ->has('id')
                                    ->where('email', $this->data["email"])
                                    ->where('name', $this->data["name"])
                                    ->where('number', (string)  $this->data["number"])
                                    ->has('is_verified')
                                    ->has('avatar')
                                    ->where('address', $this->data["address"])
                                    ->has("roles")
                                    ->has('latitude')
                                    ->has('longitude')

                            )
                    )
            );
        return  $response;
    }
}
