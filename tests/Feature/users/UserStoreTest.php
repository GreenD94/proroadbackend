<?php

namespace Tests\Feature\users;

use App\Models\User;
use Database\Seeders\MasterUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserStoreTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkStore();
    }
    public function checkData()
    {
        $this->seed(MasterUserSeeder::class);
        Storage::fake('s3');

        $file = UploadedFile::fake()->image('avatar.jpg');
        $this->userData =
            [
                "email" => $this->faker()->email(),
                "name" => null,
                "number" => random_int(100000, 999999),
                'avatar' => $file,
                "address" => $this->faker()->address(),
            ];

        $this->data = $this->userData;
        $this->assertDatabaseCount('users', 3);

        $this->createdUserModel = User::factory()->create();
        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
    }

    public function checkStore()
    {
        $response = $this->postJson(route('api.mobile.users.store'),  $this->data);

        $response->assertStatus(200);
        unset($this->userData['avatar']);
        $this->assertDatabaseCount('images', 11);
        $this->assertDatabaseHas('users', $this->userData);
        $this->assertDatabaseCount('users', 5);
        // $this->assertNotNull(User::find(3)->sms_code);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('email', $this->data["email"])
                            ->where('name', $this->data["name"])
                            ->where('number', (string) $this->data["number"])
                            ->where('is_verified', false)
                            ->has('avatar')
                            ->has('latitude')
                            ->has('longitude')
                            ->where('address', $this->data["address"])
                            ->has("roles")
                    )
            );
        $this->assertNotNull($response->original["data"]['avatar']["url"]);
    }
}
