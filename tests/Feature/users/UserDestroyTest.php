<?php

namespace Tests\Feature\users;

use App\Models\InformacionUsuario;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;


class UserDestroyTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkDestroy();
    }
    public function checkData()
    {

        $this->data =
            [
                "email" => $this->faker()->email(),
                "name" => $this->faker()->name(),
                "number" => random_int(100000, 999999),
                "address" => $this->faker()->address(),
            ];

        $this->createdUserModel = User::factory()->create($this->data);

        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->assertModelExists($this->createdUserModel);
        $this->assertDatabaseHas('users',  $this->data);
        $this->assertDatabaseCount('users', 1);
    }

    public function checkDestroy()
    {
        $params = array("id" => $this->createdUserModel->id);
        $response = $this->deleteJson(route('api.mobile.users.destroy'), $params);
        $response->assertStatus(200);
        $this->assertDatabaseCount('users', 0);

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('email', $this->data["email"])
                            ->where('name', $this->data["name"])
                            ->where('number', (string) $this->data["number"])
                            ->has('avatar')
                            ->where('address', $this->data["address"])
                            ->has('is_verified')
                            ->has("roles")
                            ->has('latitude')
                            ->has('longitude')
                    )
            );
    }
}
