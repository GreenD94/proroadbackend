<?php

namespace Tests\Feature\users;

use App\Models\InformacionUsuario;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class UserUpdateTest extends TestCase
{



    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkNombreUpdate();
        $this->checkNumeroUpdate();
        $this->checkEmailUpdate();
    }
    public function checkData()
    {
        $this->data =
            [
                "email" => $this->faker()->email(),
                "name" => $this->faker()->name(),
                "number" => random_int(100000, 999999),
                "address" => $this->faker()->address(),
            ];


        $this->data = $this->data;
        $this->createdUserModel =  User::factory()
            ->create($this->data);
        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->assertDatabaseCount('users', 1);
    }

    public function checkNombreUpdate()
    {
        $params = array('id' => $this->createdUserModel->id, "name" => "test1");
        $response = $this->putJson(route('api.mobile.users.update'), $params);
        $response->assertStatus(200);


        $this->data["name"] = $params["name"];
        $this->assertDatabaseHas('users', $this->data);
        $this->assertDatabaseCount('users', 1);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('email', $this->data["email"])
                            ->where('name', $params["name"])
                            ->has('roles')
                            ->where('number', (string) $this->data["number"])
                            ->has('avatar')
                            ->has('latitude')
                            ->has('longitude')
                            ->where('address', $this->data["address"])
                            ->has('is_verified')

                    )
            );
        $this->data = array_merge($this->data, []);
    }
    public function checkNumeroUpdate()
    {
        $params = array('id' => $this->createdUserModel->id, "number" => 21212122);
        $response = $this->putJson(route('api.mobile.users.update'), $params);
        $response->assertStatus(200);

        $this->data["number"] = $params["number"];
        $this->assertDatabaseHas('users', $this->data);
        $this->assertDatabaseCount('users', 1);

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('email', $this->data["email"])
                            ->where('name', $this->data["name"])
                            ->where('number', (string) $params["number"])
                            ->has('avatar')
                            ->where('address', $this->data["address"])
                            ->has('roles')
                            ->has('is_verified')
                            ->has('latitude')
                            ->has('longitude')

                    )
            );
        $this->data = array_merge($this->data, []);
    }
    public function checkEmailUpdate()
    {
        $params = array('id' => $this->createdUserModel->id, "email" => "test99@test99.com");
        $response = $this->putJson(route('api.mobile.users.update'), $params);
        $response->assertStatus(200);

        $this->data["email"] = $params["email"];
        $this->assertDatabaseHas('users', $this->data);
        $this->assertDatabaseCount('users', 1);

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('email',  $params["email"])
                            ->where('name', $this->data["name"])
                            ->where('number', (string)$this->data["number"])
                            ->has('avatar')
                            ->where('address', $this->data["address"])
                            ->has('avatar')
                            ->where('address', $this->data["address"])
                            ->has('roles')
                            ->has('is_verified')
                            ->has('latitude')
                            ->has('longitude')

                    )
            );
        $this->data = array_merge($this->data, []);
    }
}
