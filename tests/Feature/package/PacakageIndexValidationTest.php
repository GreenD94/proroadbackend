<?php

namespace Tests\Feature\package;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageIndexValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkPageIsIntenger();
        $this->checkPageIsNumeric();
        $this->checkPageIsNaturalNumber();

        $this->checkStatusIdExists();
    }
    public function checkData()
    {
        $authUser = User::factory()->create();
        Sanctum::actingAs(
            $authUser,
            ['*']
        );
    }
    public function checkStatusIdExists()
    {
        $params = array("page" => 1, "status_id" => 1);
        $response = $this->getJson(route('api.mobile.packages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('status_id')
                    )
            );
    }

    public function checkPageIsIntenger()
    {
        $params = array("page" => 1.1);
        $response = $this->getJson(route('api.mobile.packages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('page')
                    )
            );
    }
    public function checkPageIsNumeric()
    {
        $params = array("page" => "aaa");
        $response = $this->getJson(route('api.mobile.packages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('page')
                    )
            );
    }
    public function checkPageIsNaturalNumber()
    {
        $params = array("page" => -1);
        $response = $this->getJson(route('api.mobile.packages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('page')
                    )
            );
    }
}
