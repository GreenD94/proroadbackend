<?php

namespace Tests\Feature\package;

use App\Models\ChatRoom;
use App\Models\Package;
use App\Models\Statu;
use App\Models\StatusLog;
use App\Models\User;
use Database\Seeders\MasterUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageIndexTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkPagination();
        $this->checkStatusId();
    }
    public function checkData()
    {
        $this->seed(MasterUserSeeder::class);

        $this->createdUserModel = User::factory()->create();
        $this->data =
            [
                "chat_room_id" => ChatRoom::factory()->create()->id,
                "driver_id" => User::factory()->create()->id,
                "owner_id" => User::factory()->create()->id,
                "zip_code" => $this->faker()->randomDigit(),
                "delivery_address" => $this->faker()->address(),
                "phone" => $this->faker()->randomNumber(),
                "note" => $this->faker()->sentence(),
                "price" => $this->faker()->randomDigit(),
            ];
        $this->createdModel = Package::factory()->create($this->data);

        $models = Package::factory()->count(19)->create();
        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->createdModel->status_logs()->create(["statu_id" => Statu::find(1)->id]);
        $this->createdModel->status_logs()->create(["statu_id" => Statu::find(2)->id]);
        $models[0]->status_logs()->create(["statu_id" => Statu::find(1)->id]);
        $models[0]->status_logs()->create(["statu_id" => Statu::find(2)->id]);
        $this->assertDatabaseHas('packages', $this->data);
    }

    public function checkPagination(string $page = "1", array $params = [], $expected_size = 5, $total = 20)
    {
        $params["page"] = $page;
        $response = $this->getJson(route('api.mobile.packages.index', $params));

        $response->assertStatus(200);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json->has('total')
                            ->where('total', $total)
                            ->has('per_page')
                            ->has('current_page')
                            ->has('last_page')
                            ->has('next_page_url')
                            ->has('prev_page_url')
                            ->has(
                                'packages',
                                $expected_size,
                                fn ($json) =>
                                $json
                                    ->has('id')
                                    ->has('driver')
                                    ->has('owner')
                                    ->where('chat_room_id', $this->data["chat_room_id"])
                                    ->where('zip_code', $this->data["zip_code"])
                                    ->where('delivery_address', $this->data["delivery_address"])
                                    ->where('phone', $this->data["phone"])
                                    ->where('note', $this->data["note"])
                                    ->where('price', $this->data["price"])
                                    ->has('status')
                                    ->has('created_at')
                                    ->has('description')
                                    ->has('zip_code_pickup')
                                    ->has('delivery_pickup')
                                    ->has('images')
                                    ->has('longitude')
                                    ->has('latitude')
                                    ->has('longitude_pickup')
                                    ->has('latitude_pickup')

                            )
                    )
            );
        return  $response;
    }
    public function checkStatusId()
    {

        $response = $this->checkPagination(1, ["status_id" => 2], 2, 2);
    }
}
