<?php

namespace Tests\Feature\package;

use App\Models\ChatRoom;
use App\Models\Package;
use App\Models\Statu;
use App\Models\User;
use Database\Seeders\MasterUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageUpdateTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkChatRoomIdUpdate();
        $this->checkDriverIdUpdate();
        $this->checkOwnerIdUpdate();
        $this->checkZipCodeUpdate();
        $this->checkDeliveryAddresUpdate();
        $this->checkPhoneUpdate();
        $this->checkNoteUpdate();
        $this->checkPriceUpdate();
    }
    public function checkData()
    {
        $this->seed(MasterUserSeeder::class);
        $this->createdUserModel =  User::factory()
            ->create();

        $this->data =
            [
                "chat_room_id" => ChatRoom::factory()->create()->id,
                "driver_id" => User::factory()->create()->id,
                "owner_id" => User::factory()->create()->id,
                "zip_code" => $this->faker()->randomDigit(),
                "delivery_address" => $this->faker()->address(),
                "phone" => $this->faker()->randomNumber(),
                "note" => $this->faker()->sentence(),
                "price" => $this->faker()->randomDigit(),
            ];
        $this->createdModel = Package::factory()->create($this->data);

        $this->createdModel->status_logs()->create(["statu_id" => Statu::find(1)->id]);


        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->assertDatabaseCount('packages', 3);
    }

    public function checkChatRoomIdUpdate()
    {
        $params = array('id' => $this->createdModel->id, "chat_room_id" =>  ChatRoom::factory()->create()->id);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["chat_room_id"] = $params["chat_room_id"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')

                    )
            );
    }

    public function checkDriverIdUpdate()
    {
        $params = array('id' => $this->createdModel->id, "driver_id" =>  User::factory()->create()->id);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["driver_id"] = $params["driver_id"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')
                    )
            );
    }

    public function checkOwnerIdUpdate()
    {
        $params = array('id' => $this->createdModel->id, "owner_id" =>  User::factory()->create()->id);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["owner_id"] = $params["owner_id"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')
                    )
            );
    }

    public function checkZipCodeUpdate()
    {
        $params = array('id' => $this->createdModel->id, "zip_code" =>  1111);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["zip_code"] = $params["zip_code"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')
                    )
            );
    }
    public function checkDeliveryAddresUpdate()
    {
        $params = array('id' => $this->createdModel->id, "delivery_address" =>  "aaaaaa");
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["delivery_address"] = $params["delivery_address"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')
                    )
            );
    }
    public function checkPhoneUpdate()
    {
        $params = array('id' => $this->createdModel->id, "phone" =>  11111);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["phone"] = $params["phone"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')
                    )
            );
    }
    public function checkNoteUpdate()
    {
        $params = array('id' => $this->createdModel->id, "note" =>  "aaa");
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["note"] = $params["note"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')
                    )
            );
    }
    public function checkPriceUpdate()
    {
        $params = array('id' => $this->createdModel->id, "price" => 1111);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertStatus(200);


        $this->data["price"] = $params["price"];
        $this->assertDatabaseHas('packages', $this->data);
        $this->assertDatabaseCount('packages', 3);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')
                    )
            );
    }
}
