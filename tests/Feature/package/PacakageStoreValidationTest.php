<?php

namespace Tests\Feature\package;

use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageStoreValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $this->checkData();


        $this->checkPhoneRequired();
        $this->checkDeliveryAddresRequired();
        $this->checkPriceRequired();
        $this->checkPriceIsNumeric();




        $this->checkDriverIdExists();
        $this->checkDriverIdIsIntenger();
        $this->checkDriverIdIsNumeric();
        $this->checkDriverIdIsNaturalNumber();

        $this->checkOwnerIdRequired();
        $this->checkOwnerIdExists();
        $this->checkOwnerIdIsIntenger();
        $this->checkOwnerIdIsNumeric();
        $this->checkOwnerIdIsNaturalNumber();


        $this->checkChatRoomIdExists();
        $this->checkChatRoomIdIsIntenger();
        $this->checkChatRoomIdIsNumeric();
        $this->checkChatRoomIdIsNaturalNumber();
    }
    public function checkData()
    {
        $authUser = User::factory()->create();

        $this->data =
            [
                "chat_room_id" => ChatRoom::factory()->create()->id,
                "driver_id" => User::factory()->create()->id,
                "owner_id" => User::factory()->create()->id,
            ];

        Sanctum::actingAs(
            $authUser,
            ['*']
        );
    }

    public function checkZipCodeRequired()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "owner_id" => $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111

        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('zip_code')
                    )
            );
    }

    public function checkPhoneRequired()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "zip_code" => 11,
            "owner_id" => $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "description" => "ss",
            "price" => 1111,
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('phone')
                    )
            );
    }
    public function checkDeliveryAddresRequired()
    {
        $params = array(
            "price" => 2222,
            "zip_code" => 11,
            "owner_id" => $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('delivery_address')
                    )
            );
    }
    public function checkPriceRequired()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "zip_code" => 11,
            "owner_id" => $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('price')
                    )
            );
    }

    public function checkPriceIsNumeric()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "zip_code" => 11,
            "owner_id" => $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => "aaaaa",
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('price')
                    )
            );
    }


    public function checkOwnerIdRequired()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }
    public function checkOwnerIdExists()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "owner_id" => 22,
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }
    public function checkDriverIdIsIntenger()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "owner_id" => 1.1,
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }
    public function checkDriverIdIsNumeric()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "owner_id" => "aaaaa",
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }
    public function checkDriverIdIsNaturalNumber()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "owner_id" => -1,
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }




    public function checkDriverIdExists()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => 22,
            "owner_id" =>  $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }
    public function checkOwnerIdIsIntenger()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => 1.1,
            "owner_id" =>  $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }
    public function checkOwnerIdIsNumeric()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => "aaa",
            "owner_id" =>  $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }
    public function checkOwnerIdIsNaturalNumber()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => -1,
            "owner_id" =>  $this->data["owner_id"],
            "chat_room_id" => $this->data["chat_room_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }


    public function checkChatRoomIdRequired()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => $this->data["driver_id"],
            "owner_id" =>  $this->data["owner_id"],

            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdExists()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => $this->data["driver_id"],
            "owner_id" =>  $this->data["owner_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "chat_room_id" => 22,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsIntenger()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => $this->data["driver_id"],
            "owner_id" =>  $this->data["owner_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "chat_room_id" => 1.1,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNumeric()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => $this->data["driver_id"],
            "owner_id" =>  $this->data["owner_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "chat_room_id" => "qqq",
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNaturalNumber()
    {
        $params = array(
            "delivery_address" => "aaaaa",
            "driver_id" => $this->data["driver_id"],
            "owner_id" =>  $this->data["owner_id"],
            "phone" => 11111111,
            "price" => 1111,
            "zip_code" => 11111111,
            "chat_room_id" => -1,
            "description" => "ss",
            'delivery_pickup' => "aaaa",
            'zip_code_pickup' => 111
        );
        $response = $this->postJson(route('api.mobile.packages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
}
