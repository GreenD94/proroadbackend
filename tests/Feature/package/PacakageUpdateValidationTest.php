<?php

namespace Tests\Feature\package;

use App\Models\ChatRoom;
use App\Models\Package;
use App\Models\Statu;
use App\Models\User;
use Database\Seeders\MasterUserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageUpdateValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();

        $this->checkIdExists();
        $this->checkIdIsIntenger();
        $this->checkIdIsNumeric();
        $this->checkIdIsNaturalNumber();

        $this->checkPriceIsNumeric();




        $this->checkDriverIdExists();
        $this->checkDriverIdIsIntenger();
        $this->checkDriverIdIsNumeric();
        $this->checkDriverIdIsNaturalNumber();


        $this->checkOwnerIdExists();
        $this->checkOwnerIdIsIntenger();
        $this->checkOwnerIdIsNumeric();
        $this->checkOwnerIdIsNaturalNumber();

        $this->checkChatRoomIdExists();
        $this->checkChatRoomIdIsIntenger();
        $this->checkChatRoomIdIsNumeric();
        $this->checkChatRoomIdIsNaturalNumber();
    }

    public function checkData()
    {
        $this->seed(MasterUserSeeder::class);
        $this->data =
            [
                "chat_room_id" => ChatRoom::factory()->create()->id,
                "driver_id" => User::factory()->create()->id,
                "owner_id" => User::factory()->create()->id,
                "zip_code" => $this->faker()->randomDigit(),
                "delivery_address" => $this->faker()->address(),
                "phone" => $this->faker()->randomNumber(),
                "note" => $this->faker()->sentence(),
                "price" => $this->faker()->randomDigit(),
            ];
        $this->createdModel = Package::factory()->create($this->data);
        $this->createdModel->status_logs()->create(["statu_id" => Statu::find(1)->id]);

        $authUser = User::factory()->create();
        Sanctum::actingAs(
            $authUser,
            ['*']
        );
    }



    public function checkIdExists()
    {
        $params = array("id" => 22);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }
    public function checkIdIsIntenger()
    {
        $params = array("id" => 1.1);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }
    public function checkIdIsNumeric()
    {
        $params = array("id" => "aaa");
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }
    public function checkIdIsNaturalNumber()
    {
        $params = array("id" => -1);
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }



    public function checkPriceIsNumeric()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "price" => "aaaaa"
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('price')
                    )
            );
    }



    public function checkOwnerIdExists()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "owner_id" => 22,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);

        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }
    public function checkDriverIdIsIntenger()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "owner_id" => 1.1,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);

        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }
    public function checkDriverIdIsNumeric()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "owner_id" => "aaaaa",
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }
    public function checkDriverIdIsNaturalNumber()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "owner_id" => -1,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('owner_id')
                    )
            );
    }




    public function checkDriverIdExists()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "driver_id" => 22,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }
    public function checkOwnerIdIsIntenger()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "driver_id" => 1.1,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }
    public function checkOwnerIdIsNumeric()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "driver_id" => "aaa",
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }
    public function checkOwnerIdIsNaturalNumber()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "driver_id" => -1,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('driver_id')
                    )
            );
    }



    public function checkChatRoomIdExists()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "chat_room_id" => 22,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsIntenger()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "chat_room_id" => 1.1,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNumeric()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "chat_room_id" => "aaaa",
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNaturalNumber()
    {
        $params = array(
            "id" =>  $this->createdModel->id,
            "chat_room_id" => -1,
        );
        $response = $this->putJson(route('api.mobile.packages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
}
