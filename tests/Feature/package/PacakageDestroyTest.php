<?php

namespace Tests\Feature\package;

use App\Models\ChatRoom;
use App\Models\Package;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageDestroyTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkDestroy();
    }
    public function checkData()
    {

        $this->createdUserModel = User::factory()->create();
        $this->data =
            [
                "chat_room_id" => ChatRoom::factory()->create()->id,
                "driver_id" => User::factory()->create()->id,
                "owner_id" => User::factory()->create()->id,
                "zip_code" => $this->faker()->randomDigit(),
                "delivery_address" => $this->faker()->address(),
                "phone" => $this->faker()->randomNumber(),
                "note" => $this->faker()->sentence(),
                "price" => $this->faker()->randomDigit(),
            ];
        $this->createdModel = Package::factory()->create($this->data);


        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->assertModelExists($this->createdModel);
        $this->assertDatabaseCount('packages', 1);
    }

    public function checkDestroy()
    {
        $params = array("id" => $this->createdModel->id);
        $response = $this->deleteJson(route('api.mobile.packages.destroy'), $params);
        $response->assertStatus(200);
        $this->assertDatabaseCount('packages', 0);

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->has('driver')
                            ->has('owner')
                            ->where('chat_room_id', $this->data["chat_room_id"])
                            ->where('zip_code', $this->data["zip_code"])
                            ->where('delivery_address', $this->data["delivery_address"])
                            ->where('phone', $this->data["phone"])
                            ->where('note', $this->data["note"])
                            ->where('price', $this->data["price"])
                            ->has('status')
                            ->has('created_at')
                            ->has('description')
                            ->has('zip_code_pickup')
                            ->has('delivery_pickup')
                            ->has('images')
                            ->has('longitude')
                            ->has('latitude')
                            ->has('longitude_pickup')
                            ->has('latitude_pickup')

                    )
            );
    }
}
