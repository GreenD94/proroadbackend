<?php

namespace Tests\Feature\pacakage_status;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageStatusStoreValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkNameIsRequired();
    }
    public function checkData()
    {
        $authUser = User::factory()->create();
        $this->data =
            [
                "name" => $this->faker()->word(),
            ];

        Sanctum::actingAs(
            $authUser,
            ['*']
        );
    }
    public function checkNameIsRequired()
    {
        $params = array();
        $response = $this->postJson(route('api.mobile.package_status.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('name')
                    )
            );
    }
}
