<?php

namespace Tests\Feature\pacakage_status;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageStatusStoreTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkStore();
    }
    public function checkData()
    {
        $this->data =
            [
                "name" => $this->faker()->word(),

            ];
        $this->assertDatabaseCount('status', 0);

        $this->createdUserModel = User::factory()->create();
        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
    }

    public function checkStore()
    {
        $response = $this->postJson(route('api.mobile.package_status.store'),  $this->data);

        $response->assertStatus(200);
        $this->assertDatabaseHas('status', $this->data);
        $this->assertDatabaseCount('status', 1);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('name', $this->data["name"])
                    )
            );
    }
}
