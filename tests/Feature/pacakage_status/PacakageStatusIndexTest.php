<?php

namespace Tests\Feature\pacakage_status;

use App\Models\Statu;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageStatusIndexTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkPagination();
    }
    public function checkData()
    {

        $this->createdUserModel = User::factory()->create();

        $this->data =
            [
                "name" => $this->faker()->name(),
            ];
        $this->createdModel = Statu::factory()->create($this->data);
        Statu::factory()->count(19)->create();
        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );

        $this->assertDatabaseHas('status', $this->data);
        $this->assertDatabaseCount('status', 20);
    }

    public function checkPagination(string $page = "1", array $params = [], $expected_size = 5, $total = 20)
    {
        $params["page"] = $page;
        $response = $this->getJson(route('api.mobile.package_status.index', $params));

        $response->assertStatus(200);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json->has('total')
                            ->where('total', $total)
                            ->has('per_page')
                            ->has('current_page')
                            ->has('last_page')
                            ->has('next_page_url')
                            ->has('prev_page_url')
                            ->has(
                                'package_status',
                                $expected_size,
                                fn ($json) =>
                                $json
                                    ->has('id')
                                    ->where('name', $this->data["name"])
                            )
                    )
            );
        return  $response;
    }
}
