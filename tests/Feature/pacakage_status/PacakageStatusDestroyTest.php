<?php

namespace Tests\Feature\package_status;

use App\Models\Statu;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PacakageStatusDestroyTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkDestroy();
    }
    public function checkData()
    {

        $this->createdUserModel = User::factory()->create();
        $this->data =
            [
                "name" => $this->faker()->word(),
            ];
        $this->createdModel = Statu::factory()->create($this->data);
        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->assertModelExists($this->createdModel);
        $this->assertDatabaseHas('status', $this->data);
        $this->assertDatabaseCount('status', 1);
    }

    public function checkDestroy()
    {
        $params = array("id" => $this->createdModel->id);
        $response = $this->deleteJson(route('api.mobile.package_status.destroy'), $params);
        $response->assertStatus(200);
        $this->assertDatabaseCount('status', 0);

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('name', $this->data["name"])
                    )
            );
    }
}
