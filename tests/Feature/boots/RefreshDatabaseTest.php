<?php

namespace Tests\Feature\boots;

use App\Models\Banner;
use App\Models\Category;
use App\Models\Color;
use App\Models\Image;
use App\Models\Product;
use App\Models\Size;
use App\Models\Stock;
use App\Models\User;
use App\Models\Video;
use Database\Seeders\MasterUserSeeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Testing\TestResponse;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class RefreshDatabaseTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        // $this->checkData();
        // $this->checkindex();
    }

    public function checkData(): void
    {

        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );


        $this->assertDatabaseCount('status',  0);
    }

    public function checkindex(): void
    {
        $response = $this->getJson(route('api.mobile.boots.refresh'));
        dd($response->original);
        $response->assertStatus(200);
        $this->assertDatabaseCount('status', 3);
    }
}
