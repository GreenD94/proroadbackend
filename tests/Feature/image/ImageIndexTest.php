<?php

namespace Tests\Feature\image;

use App\Models\Image;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ImageIndexTest extends TestCase
{

    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        Storage::fake('s3');



        $modelData = [
            'url' => $this->faker->imageUrl(),
            'name' => $this->faker->userName()
        ];
        $model = Image::factory()->create($modelData);

        Image::factory()->count(9)->create();
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        $this->assertDatabaseCount('images', 11);
        $this->assertDatabaseHas('images', $modelData);

        $response = $this->getJson(route('api.mobile.images.index', ['page' => 1]));


        $response->assertStatus(200);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json->where('total', 11)
                            ->has('total')
                            ->has('per_page')
                            ->has('current_page')
                            ->has('last_page')
                            ->has('next_page_url')
                            ->has('prev_page_url')
                            ->has(
                                'images',
                                5,
                                fn ($json) =>  $json
                                    ->where('url', (Storage::disk('s3')->url($model['url'])))
                                    ->where('name', $model['name'])
                                    ->where('id', $model['id'])
                            )
                    )
            );
    }
}
