<?php

namespace Tests\Feature\chat_room;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\api_models\ChatRoomsApi;
use Tests\CheckHelpers;
use Tests\TestCase;

class ChatRoomIndexTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CheckHelpers;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkindex();
    }
    public function checkData(): void
    {
        $this->initdata(ChatRoomsApi::index());
        $this->auhUser = $this->SanctumActingAs();
        $this->data = [
            "is_active" => true,
        ];
        $this->createdModel = $this->mainModelCreate($this->data, ["chat_rooms" => 1]);
        $this->mainModelCreateMany([],   ["chat_rooms" => 20], 19);
        $this->data["data_length"] = 5;
    }
    public function checkindex(): void
    {
        $params = array('page' => 1);
        $response = $this->CallIndexApi(null, $params);

        $this->data["id"] = $this->createdModel->id;
        $this->ResponseAssertJson($response, $this->data);
    }
}
