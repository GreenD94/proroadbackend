<?php

namespace Tests\Feature\chat_room;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\api_models\ChatRoomsApi;
use Tests\CheckHelpers;
use Tests\TestCase;

class ChatRoomStoreValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CheckHelpers;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $this->checkData();

        $this->CheckFieldIsRequired("is_active",  $this->data);
        $this->CheckFieldIsBoolean("is_active",  $this->data);
    }
    public function checkData()
    {

        $this->initdata(ChatRoomsApi::store());
        $this->auhUser = $this->SanctumActingAs();
        $this->data = [
            "is_active" => true,
        ];
        $this->createdModel = $this->mainModelCreate($this->data, ["chat_rooms" => 1]);
    }
}
