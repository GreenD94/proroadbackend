<?php

namespace Tests\Feature\chat_room;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\api_models\ChatRoomsApi;
use Tests\CheckHelpers;
use Tests\TestCase;

class ChatRoomIndexValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CheckHelpers;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $params = array(
            'page' => 1,
        );

        $this->CheckFieldIsRequired("page", $params);
        $this->checkFieldIsIntenger("page", $params);
        $this->checkFieldIsNumeric("page", $params);
        $this->checkFieldIsNaturalNumber("page", $params);
    }
    public function checkData()
    {
        $this->initdata(ChatRoomsApi::index());
        $this->auhUser = $this->SanctumActingAs();
        $this->data = [
            "is_active" => true,
        ];
        $this->createdModel = $this->mainModelCreate($this->data, ["chat_rooms" => 1]);
        $this->mainModelCreateMany([],   ["chat_rooms" => 20], 19);
    }
}
