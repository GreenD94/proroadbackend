<?php

namespace Tests\Feature\chat_room;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\api_models\ChatRoomsApi;
use Tests\CheckHelpers;
use Tests\TestCase;

class ChatRoomDestroyValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CheckHelpers;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $params = array("id" => $this->createdModel->id);
        $this->CheckFieldIsRequired("id", $params);
        $this->checkFieldIsExists("id", $params);
        $this->checkFieldIsIntenger("id", $params);
        $this->checkFieldIsNumeric("id", $params);
        $this->checkFieldIsNaturalNumber("id", $params);
    }
    public function checkData()
    {
        $this->initdata(ChatRoomsApi::destroy());
        $this->auhUser = $this->SanctumActingAs();
        $this->data = [
            "is_active" => true,
        ];
        $this->createdModel = $this->mainModelCreate($this->data, ["chat_rooms" => 1]);
    }
}
