<?php

namespace Tests\Feature\chat_room;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\api_models\ChatRoomsApi;
use Tests\CheckHelpers;
use Tests\TestCase;

class ChatRoomUpdateTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CheckHelpers;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();

        $this->checkUpdateIsMain();
    }
    public function checkData(): void
    {
        $this->initdata(ChatRoomsApi::update());
        $this->auhUser = $this->SanctumActingAs();
        $this->data = [
            "is_active" => true,
        ];
        $this->createdModel = $this->mainModelCreate($this->data, ["chat_rooms" => 1]);
        $this->data['id'] = $this->createdModel->id;
    }

    public function checkUpdateIsMain()
    {

        $this->data["is_active"] = false;
        $params = array('id' => $this->createdModel->id, "is_active" => $this->data["is_active"]);
        $response = $this->CallUpdateApi(null,  $params, ["chat_rooms" => 1]);
        $this->ResponseAssertJson($response, $this->data);
    }
}
