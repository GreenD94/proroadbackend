<?php

namespace Tests\Feature\chat_message;

use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ChatMessageUpdateValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();

        $this->checkIdExists();
        $this->checkIdIsIntenger();
        $this->checkIdIsNumeric();
        $this->checkIdIsNaturalNumber();

        $this->checkIsCheckIsBoolean();
        $this->checkIsDoubleCheckIsBoolean();


        $this->checkUserIdExists();
        $this->checkUserIdIsIntenger();
        $this->checkUserIdIsNumeric();
        $this->checkUserIdIsNaturalNumber();


        $this->checkChatRoomIdExists();
        $this->checkChatRoomIdIsIntenger();
        $this->checkChatRoomIdIsNumeric();
        $this->checkChatRoomIdIsNaturalNumber();
    }

    public function checkData()
    {
        $authUser = User::factory()->create();
        $chatRoom = ChatRoom::factory()->create();
        $this->data =
            [
                "chat_room_id" =>  $chatRoom->id,
                "user_id" => $authUser->id,
            ];
        $this->chatMessage = ChatMessage::factory()->create($this->data);
        Sanctum::actingAs(
            $authUser,
            ['*']
        );
    }



    public function checkIdExists()
    {
        $params = array("id" => 22);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }
    public function checkIdIsIntenger()
    {
        $params = array("id" => 1.1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }
    public function checkIdIsNumeric()
    {
        $params = array("id" => "aaa");
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }
    public function checkIdIsNaturalNumber()
    {
        $params = array("id" => -1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                    )
            );
    }





    public function checkIsCheckIsBoolean()
    {
        $params = array("is_check" => "aaaa", "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('is_check')
                    )
            );
    }
    public function checkIsDoubleCheckIsBoolean()
    {
        $params = array("is_double_check" => "aaaa", "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('is_double_check')
                    )
            );
    }

    public function checkUserIdExists()
    {
        $params = array("user_id" => 22, "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }
    public function checkUserIdIsIntenger()
    {
        $params = array("user_id" => 1.1, "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }
    public function checkUserIdIsNumeric()
    {
        $params = array("user_id" => "aaa", "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }
    public function checkUserIdIsNaturalNumber()
    {
        $params = array("user_id" => -1, "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }


    public function checkChatRoomIdExists()
    {
        $params = array("chat_room_id" => 22, "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsIntenger()
    {
        $params = array("chat_room_id" => 1.1, "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNumeric()
    {
        $params = array("chat_room_id" => "aaaaaa", "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNaturalNumber()
    {
        $params = array("chat_room_id" => -1, "id" => 1);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
}
