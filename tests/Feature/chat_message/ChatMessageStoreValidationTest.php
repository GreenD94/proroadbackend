<?php

namespace Tests\Feature\chat_message;

use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ChatMessageStoreValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();

        $this->checkMessageRequired();
        $this->checkIsCheckIsBoolean();
        $this->checkIsDoubleCheckIsBoolean();

        $this->checkUserIdRequired();
        $this->checkUserIdExists();
        $this->checkUserIdIsIntenger();
        $this->checkUserIdIsNumeric();
        $this->checkUserIdIsNaturalNumber();

        $this->checkChatRoomIdRequired();
        $this->checkChatRoomIdExists();
        $this->checkChatRoomIdIsIntenger();
        $this->checkChatRoomIdIsNumeric();
        $this->checkChatRoomIdIsNaturalNumber();
    }
    public function checkData()
    {
        $authUser = User::factory()->create();
        $chatRoom = ChatRoom::factory()->create();
        $this->data =
            [
                "chat_room_id" =>  $chatRoom->id,
                "user_id" => $authUser->id,
            ];

        Sanctum::actingAs(
            $authUser,
            ['*']
        );
    }

    public function checkMessageRequired()
    {
        $params = array("user_id" => $this->data["user_id"], "chat_room_id" => $this->data["chat_room_id"]);
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('message')
                    )
            );
    }

    public function checkIsCheckIsBoolean()
    {
        $params = array("is_check" => "aaaa", "user_id" => $this->data["user_id"], "chat_room_id" => $this->data["chat_room_id"], "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('is_check')
                    )
            );
    }
    public function checkIsDoubleCheckIsBoolean()
    {
        $params = array("is_double_check" => "aaaa", "user_id" => $this->data["user_id"], "chat_room_id" => $this->data["chat_room_id"], "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('is_double_check')
                    )
            );
    }
    public function checkUserIdRequired()
    {
        $params = array("user_id" => 22, "chat_room_id" => $this->data["chat_room_id"], "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }
    public function checkUserIdExists()
    {
        $params = array("user_id" => 22, "chat_room_id" => $this->data["chat_room_id"], "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }
    public function checkUserIdIsIntenger()
    {
        $params = array("user_id" => 1.1, "chat_room_id" => $this->data["chat_room_id"], "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }
    public function checkUserIdIsNumeric()
    {
        $params = array("user_id" => "aaa", "chat_room_id" => $this->data["chat_room_id"], "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }
    public function checkUserIdIsNaturalNumber()
    {
        $params = array("user_id" => -1, "chat_room_id" => $this->data["chat_room_id"], "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('user_id')
                    )
            );
    }

    public function checkChatRoomIdRequired()
    {
        $params = array("user_id" => $this->data["user_id"],  "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdExists()
    {
        $params = array("user_id" => $this->data["user_id"], "chat_room_id" => 22, "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsIntenger()
    {
        $params = array("user_id" => $this->data["user_id"], "chat_room_id" => 1.1, "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNumeric()
    {
        $params = array("user_id" => $this->data["user_id"], "chat_room_id" => "aaaaaa", "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNaturalNumber()
    {
        $params = array("user_id" => $this->data["user_id"], "chat_room_id" => -1, "message" => "aaaaaaaa");
        $response = $this->postJson(route('api.mobile.chat_messages.store'), $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
}
