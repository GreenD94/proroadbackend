<?php

namespace Tests\Feature\chat_message;

use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ChatMessageIndexTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkPagination();
        $this->checkChatRoomId();
    }
    public function checkData()
    {
        Storage::fake('s3');
        $this->createdUserModel = User::factory()->create();
        $chatRoom = ChatRoom::factory()->create();
        $this->data =
            [
                "message" => $this->faker()->email(),
                "is_check" => false,
                "is_double_check" => false,
                "chat_room_id" =>  $chatRoom->id,
                "user_id" => $this->createdUserModel->id,
            ];
        $this->createdModel = ChatMessage::factory()->create($this->data);
        $this->otherChatRoom = ChatMessage::factory()->create();
        ChatMessage::factory()->count(18)->create([
            "chat_room_id" =>  $chatRoom->id,
            "user_id" => $this->createdUserModel->id,
        ]);
        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );

        $this->assertDatabaseHas('chat_messages', $this->data);
        $this->assertDatabaseCount('chat_messages', 20);
        $this->assertDatabaseCount('chat_rooms', 2);
    }

    public function checkPagination(string $page = "1", array $params = [], $expected_size = 5, $total = 20, $no_compare = false)
    {
        $params["page"] = $page;
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));

        $response->assertStatus(200);
        if ($no_compare) return $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json->has('total')
                            ->where('total', $total)
                            ->has('per_page')
                            ->has('current_page')
                            ->has('last_page')
                            ->has('next_page_url')
                            ->has('prev_page_url')
                            ->has(
                                'chat_messages',
                                $expected_size,
                                fn ($json) =>
                                $json
                                    ->has('id')
                                    ->has('message')
                                    ->has('is_check')
                                    ->has('is_double_check')
                                    ->has('user')
                                    ->has('image')

                            )
                    )
            );
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json) =>
                        $json->has('total')
                            ->where('total', $total)
                            ->has('per_page')
                            ->has('current_page')
                            ->has('last_page')
                            ->has('next_page_url')
                            ->has('prev_page_url')
                            ->has(
                                'chat_messages',
                                $expected_size,
                                fn ($json) =>
                                $json
                                    ->has('id')
                                    ->where('message', $this->data["message"])
                                    ->where('is_check', $this->data["is_check"])
                                    ->where('is_double_check', $this->data["is_double_check"])
                                    ->has('user')
                                    ->has('image')

                            )
                    )
            );
        return  $response;
    }
    public function checkChatRoomId()
    {
        $params = array("chat_room_id" =>   $this->otherChatRoom->chat_room_id);
        $result = $this->checkPagination(1, $params, 1, 1, true);
    }
}
