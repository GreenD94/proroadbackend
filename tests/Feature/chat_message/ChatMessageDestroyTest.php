<?php

namespace Tests\Feature\chat_message;

use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ChatMessageDestroyTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkDestroy();
    }
    public function checkData()
    {
        Storage::fake('s3');
        $this->createdUserModel = User::factory()->create();
        $chatRoom = ChatRoom::factory()->create();
        $this->data =
            [
                "message" => $this->faker()->email(),
                "is_check" => false,
                "is_double_check" => false,
                "chat_room_id" =>  $chatRoom->id,
                "user_id" => $this->createdUserModel->id,
            ];
        $this->createdModel = ChatMessage::factory()->create($this->data);


        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->assertModelExists($this->createdModel);
        $this->assertDatabaseHas('chat_messages', $this->data);
        $this->assertDatabaseCount('chat_messages', 1);
        $this->assertDatabaseCount('chat_rooms', 1);
    }

    public function checkDestroy()
    {
        $params = array("id" => $this->createdModel->id);
        $response = $this->deleteJson(route('api.mobile.chat_messages.destroy'), $params);
        $response->assertStatus(200);
        $this->assertDatabaseCount('chat_messages', 0);

        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('message', $this->data["message"])
                            ->where('is_check', $this->data["is_check"])
                            ->where('is_double_check', $this->data["is_double_check"])
                            ->has('user')
                            ->has('image')
                    )
            );
    }
}
