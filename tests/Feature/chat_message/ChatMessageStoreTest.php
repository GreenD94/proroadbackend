<?php

namespace Tests\Feature\chat_message;

use App\Models\ChatRoom;
use App\Models\Image;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ChatMessageStoreTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkStore();
        $this->checkChatUsers();
        $this->checkChatUsers();
        $this->assertDatabaseCount('chat_users', 4);
    }
    public function checkData()
    {
        Storage::fake('s3');
        $this->createdUserModel = User::factory()->create();
        $this->chatRoom = ChatRoom::factory()->create();
        $this->data =
            [
                "message" => $this->faker()->email(),
                "is_check" => false,
                "is_double_check" => false,
                "chat_room_id" =>  $this->chatRoom->id,
                "user_id" => $this->createdUserModel->id,
                "image_id" => Image::factory()->create()->id
            ];
        $this->assertDatabaseCount('users', 1);
        $this->assertDatabaseCount('chat_rooms', 1);


        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->users = User::factory()->count(3)->create();
        $this->chatRoom->users()->sync([$this->users[0]->id, $this->users[1]->id, $this->users[2]->id]);
    }

    public function checkStore()
    {
        $response = $this->postJson(route('api.mobile.chat_messages.store'),  $this->data);


        $response->assertStatus(200);
        $this->assertDatabaseHas('chat_messages', $this->data);
        $this->assertDatabaseCount('chat_messages', 1);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('message', $this->data["message"])
                            ->where('is_check', $this->data["is_check"])
                            ->where('is_double_check', $this->data["is_double_check"])
                            ->has('user')
                            ->has('image')
                    )
            );
    }
    public function checkChatUsers()
    {

        $this->data["is_chat_users"] = true;
        $response = $this->postJson(route('api.mobile.chat_messages.store'),  $this->data);

        $response->assertStatus(200);



        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        4,
                        fn ($json1) =>
                        $json1
                            ->where('id', $this->createdUserModel->id)
                            ->has('number')
                            ->etc()
                    )
            );
    }
}
