<?php

namespace Tests\Feature\chat_message;

use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ChatMessageUpdateTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $this->checkData();
        $this->checkMessageUpdate();
        $this->checkIsCheckUpdate();
        $this->checkIsDoubleCheckUpdate();
    }
    public function checkData()
    {
        Storage::fake('s3');
        $this->createdUserModel =  User::factory()
            ->create();
        $chatRoom = ChatRoom::factory()->create();
        $this->data =
            [
                "message" => $this->faker()->email(),
                "is_check" => false,
                "is_double_check" => false,
                "chat_room_id" =>  $chatRoom->id,
                "user_id" => $this->createdUserModel->id,
            ];
        $this->createdModel = ChatMessage::factory()->create($this->data);


        Sanctum::actingAs(
            $this->createdUserModel,
            ['*']
        );
        $this->assertDatabaseCount('users', 1);
        $this->assertDatabaseCount('chat_rooms', 1);
        $this->assertDatabaseCount('chat_messages', 1);
    }

    public function checkMessageUpdate()
    {
        $params = array('id' => $this->createdModel->id, "message" => "test1");
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertStatus(200);


        $this->data["message"] = $params["message"];
        $this->assertDatabaseHas('chat_messages', $this->data);
        $this->assertDatabaseCount('chat_messages', 1);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('message', $params["message"])
                            ->where('is_check', $this->data["is_check"])
                            ->where('is_double_check', $this->data["is_double_check"])
                            ->has('user')
                            ->has('image')

                    )
            );
    }

    public function checkIsCheckUpdate()
    {
        $params = array('id' => $this->createdModel->id, "is_check" => true);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertStatus(200);


        $this->data["is_check"] = $params["is_check"];
        $this->assertDatabaseHas('chat_messages', $this->data);
        $this->assertDatabaseCount('chat_messages', 1);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('message', $this->data["message"])
                            ->where('is_check', $params["is_check"])
                            ->where('is_double_check', $this->data["is_double_check"])
                            ->has('user')
                            ->has('image')

                    )
            );
    }

    public function checkIsDoubleCheckUpdate()
    {
        $params = array('id' => $this->createdModel->id, "is_double_check" => true);
        $response = $this->putJson(route('api.mobile.chat_messages.update'), $params);
        $response->assertStatus(200);


        $this->data["is_double_check"] = $params["is_double_check"];
        $this->assertDatabaseHas('chat_messages', $this->data);
        $this->assertDatabaseCount('chat_messages', 1);
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('id')
                            ->where('message', $this->data["message"])
                            ->where('is_check', $this->data["is_check"])
                            ->where('is_double_check', $params["is_double_check"])
                            ->has('user')
                            ->has('image')

                    )
            );
    }
}
