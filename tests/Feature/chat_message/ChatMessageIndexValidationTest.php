<?php

namespace Tests\Feature\chat_message;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ChatMessageIndexValidationTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->checkData();
        $this->checkPageIsIntenger();
        $this->checkPageIsNumeric();
        $this->checkPageIsNaturalNumber();



        $this->checkChatRoomIdExists();
        $this->checkChatRoomIdIsIntenger();
        $this->checkChatRoomIdIsNumeric();
        $this->checkChatRoomIdIsNaturalNumber();
    }
    public function checkData()
    {
        $authUser = User::factory()->create();
        Sanctum::actingAs(
            $authUser,
            ['*']
        );
    }

    public function checkPageIsIntenger()
    {
        $params = array("page" => 1.1);
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('page')
                    )
            );
    }
    public function checkPageIsNumeric()
    {
        $params = array("page" => "aaa");
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('page')
                    )
            );
    }
    public function checkPageIsNaturalNumber()
    {
        Storage::fake('s3');
        $params = array("page" => -1);
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('page')
                    )
            );
    }


    public function checkChatRoomIdExists()
    {
        $params = array("chat_room_id" => 22, "page" => 1);
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsIntenger()
    {
        $params = array("page" => 1, "chat_room_id" => 1.1, "page" => 1);
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNumeric()
    {
        $params = array("page" => 1, "chat_room_id" => "aaaaaa",);
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
    public function checkChatRoomIdIsNaturalNumber()
    {
        $params = array("page" => 1, "chat_room_id" => -1);
        $response = $this->getJson(route('api.mobile.chat_messages.index', $params));
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has('chat_room_id')
                    )
            );
    }
}
