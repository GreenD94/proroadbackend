<?php

namespace Tests;

use App\Models\Image;
use App\Models\User;
use Exception;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\assertTrue;

trait CheckHelpers
{

    protected $mainModelName, $mainApiName, $mainApiMethod, $mainApiModelName;



    protected function initdata(array $data)
    {
        $this->mainModelName = $data["mainModelName"];
        $this->mainApiName = $data["mainApiName"];
        $this->mainApiMethod = $data["mainApiMethod"];
        $this->mainApiModelName = $data["mainApiModelName"];
    }
    protected function ResponseAssertJson(\Illuminate\Testing\TestResponse $response, array $controlData)
    {
        $mainApiModelName =  $this->mainApiModelName;

        $is_sucees = $mainApiModelName::checkResponse($this->mainApiName, $response, $controlData);
        if (!$is_sucees) $this->assertTrue(false, "test route did not match with any use case");
    }

    protected function SanctumActingAs(array $userData = [], array $abilities =  ['*']): User
    {
        $authdUser = User::factory()->create($userData);
        Sanctum::actingAs(
            $authdUser,
            $abilities
        );
        return  $authdUser;
    }

    protected function storageS3(): \Illuminate\Http\Testing\File
    {
        Storage::fake('s3');
        $file = UploadedFile::fake()->image('avatar.jpg');
        return  $file;
    }


    protected function mainModelCreate(array $modelData = [], array $tableNames = [], $is_dd = false)
    {
        $model = $this->mainModelName;
        $createdModel = $model::Factory()->create($modelData);
        if ($is_dd) dd(Image::all());
        foreach ($tableNames as $key => $value) {
            $this->assertDatabaseCount($key, $value);
        }
        return $createdModel;
    }
    protected function mainModelCreateMany(array $modelData = [], array $tableNames = [], int $count)
    {
        $model = $this->mainModelName;
        $createdModels = $model::Factory()->count($count)->create($modelData);
        foreach ($tableNames as $key => $value) {
            $this->assertDatabaseCount($key, $value);
        }
        return $createdModels;
    }
    protected function CallDestroyApi(String  $mainApiName = null, array $params = [], $tableNames = [], int $status = 200): \Illuminate\Testing\TestResponse
    {
        $routeUrl = $mainApiName ?? $this->mainApiName;

        $response = $this->deleteJson(route($routeUrl), $params);

        $response->assertStatus($status);
        foreach ($tableNames as $key => $value) {
            $this->assertDatabaseCount($key, $value);
        }
        return $response;
    }
    protected function CallStoreApi(String  $mainApiName = null, array $params = [], $tableNames = [], int $status = 200): \Illuminate\Testing\TestResponse
    {
        $routeUrl = $mainApiName ?? $this->mainApiName;
        $response = $this->postJson(route($routeUrl), $params);
        $response->assertStatus($status);
        foreach ($tableNames as $key => $value) {
            $this->assertDatabaseCount($key, $value);
        }
        return $response;
    }
    protected function CallUpdateApi(String  $mainApiName = null, array $params = [], $tableNames = [], int $status = 200): \Illuminate\Testing\TestResponse
    {
        $routeUrl = $mainApiName ?? $this->mainApiName;
        $response = $this->putJson(route($routeUrl), $params);
        $response->assertStatus($status);
        foreach ($tableNames as $key => $value) {
            $this->assertDatabaseCount($key, $value);
        }
        return $response;
    }
    protected function CallIndexApi(String  $mainApiName = null, array $params = [], $tableNames = [], int $status = 200): \Illuminate\Testing\TestResponse
    {
        $routeUrl = $mainApiName ?? $this->mainApiName;
        $response = $this->getJson(route($routeUrl, $params));
        $response->assertStatus($status);
        foreach ($tableNames as $key => $value) {
            $this->assertDatabaseCount($key, $value);
        }
        return $response;
    }


    protected function CallMAinApi(string $mainApiMethod, array $params = []): \Illuminate\Testing\TestResponse
    {
        $routeUrl = $this->mainApiName;
        $response = null;
        switch ($mainApiMethod) {
            case 'post':
                $response = $this->postJson(route($routeUrl), $params);
                break;
            case 'delete':
                $response = $this->deleteJson(route($routeUrl), $params);
                break;
            case 'put':
                $response = $this->putJson(route($routeUrl), $params);
                break;
            case 'get':
                $response = $this->getJson(route($routeUrl, $params));
                break;
            default:
                # code...
                break;
        }

        return $response;
    }
    protected function CheckFieldIsExists(string $field, array $params = [])
    {

        $params[$field] = 44444;
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
    protected function CheckFieldIsRequired(string $field, array $params = [], $is_dd = false)
    {

        unset($params[$field]);
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);

        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
    protected function CheckFieldIsIntenger(string $field, array $params = [])
    {

        $params[$field] = 1.1;
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
    protected function CheckFieldIsNumeric(string $field, array $params = [])
    {

        $params[$field] = "aaaaaa";
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
    protected function CheckFieldIsNaturalNumber(string $field, array $params = [])
    {

        $params[$field] = -1;
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
    protected function CheckFieldIsUnique(string $field, array $params = [])
    {
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
    protected function CheckFieldIsBoolean(string $field, array $params = [])
    {
        $params[$field] = 'aaaaa';
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
    protected function CheckFieldIsImage(string $field, array $params = [])
    {
        $params[$field] = 'aaaaa';
        $response = $this->CallMAinApi($this->mainApiMethod ?? "post", $params);
        $response->assertUnprocessable();
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has('message')
                    ->has(
                        'data',
                        fn ($json1) =>
                        $json1
                            ->has($field)
                    )
            );
    }
}
